module.exports = {
  appList : ["bentanayan", "zcommerce", "GPRS", "UPS"],

  defTokenExpires : 1000 * 60 * 60 * 24 * 7,

  fbProfileUrl : ["https://graph.facebook.com/v2.4/me?",
    "fields=id,email,first_name,middle_name,last_name,",
    "gender,birthday,picture.type(large),cover",
    "&access_token="].join(""),

  hosts : ["localhost", "api.bentanayan.com"],

  paymentPlatformList : ["paypal", "ecash"]
}
