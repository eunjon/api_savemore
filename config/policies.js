/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {
  "*": ["checkApp", "isAuthorized"],

  BrandsController : {
    find : "checkApp"
  },

  CurrenciesController : {
    create : false,
    update : false,
    destroy : false
  },

  MiscController : {
    create : false,
    find : "checkApp",
    update : false,
    destroy : false
  },

  OrdersController : {
    create : false,
    destroy : false
  },

  ProductsController : {
    find : "checkApp",
    findOne : "checkApp"
  },

  ReviewsController : {
    find : false,
    findOne : false
  },

  RoomsController : {
    create : false,
    update : false,
    destroy : false
  },

  SessionsController : {
    "*" : false,
    create : "checkApp",
    twitter : "checkApp",
    twitteroAuthResponse : "checkApp"
  },

  ShopsController : {
    update : false,
    destroy : false
  },

  UsersController : {
    "*" :  ["checkApp", "isAuthorized"],
    create : "checkApp",
    findOne : "checkApp",
    destroy : false,
    resetPassword : true,
    setPassword : true,
    verifyToken : true
  },
};
