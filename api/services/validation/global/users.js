var moment = require("moment");

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Global :: Users");
  sails.log.verbose(values);

  try {
    if (isUpdate) {
      if (values.password && values.confirmPassword && values.password !== values.confirmPassword) {
        throw new Error();
      }
    } else if ((!values.password || values.password !== values.confirmPassword) && values.registrationType === "native") {
      throw new Error();
    }
  } catch (e) {
    return process.nextTick(function () {
      callback(ErrorService.validation({
        password : [{
          rule : "password",
          message : "Password is blank or doesn't match."
        }]
      }));
    });
  }

  if (values.birthday && !moment(new Date(values.birthday)).isValid()) {
    return process.nextTick(function () {
      callback(ErrorService.validation({
        birthday : [{
          rule : "valid",
          message : "Birthday is not a valid date."
        }]
      }));
    });
  }

  _.each(sails.config.appList, function(app) {
    _.ensure(values, "metadata." + app, {});
  });

  async.series([
    function (seriesCb) {
      checkEmail(values, seriesCb);
    },
    function (seriesCb) {
      addCurrency(values, seriesCb);
    },
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

/**
 * Checks if email is already taken
 *
 * @method     checkEmail
 * @param      {object}    values    { description }
 * @param      {Function}  callback  { description }
 * @return     {<type>}    { description_of_the_return_value }
 */
function checkEmail (values, callback) {
  _.ensure(values, "__old.email", "");
  if (!values.email && values.registrationType !== "native") {
    return process.nextTick(callback);
  } else if (values.email === values.__old.email) {
    return process.nextTick(callback);
  }

  Users.count({
    email : values.email
  }).exec(function(err, count) {
    if (err) {
      return callback(err);
    } else if (count === 1) {
      return callback(ErrorService.validation({
        email : [{
          rule : "unique",
          message : "Email address is already taken."
        }]
      }));
    } else if (count > 1) {
      sails.log.error("Multiple users using same email.");
    }

    callback();
  });
}

/**
 * Check is username is already taken
 *
 * @method     checkUsername
 * @param      {object}    values    { description }
 * @param      {Function}  callback  { description }
 * @return     {<type>}    { description_of_the_return_value }
 */
function checkUsername (values, callback) {
  if (!values.username && values.registrationType !== "native") {
    return process.nextTick(callback);
  }

  Users.findOne({
    username : values.username
  }).exec(function (err, user) {
    if (err) {
      return callback(err);
    } else if (user && user.id === values.id) {
      return callback();
    } else if (user) {
      return callback(ErrorService.validation({
        username : [{
          rule : "unique",
          message : "Username is already taken."
        }]
      }));
    }
  });
}

/**
 * Adds currency id to the user object
 *
 * @method     addCurrency
 * @param      {object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function addCurrency (values, callback) {
  var code = "USD";

  if (values.appOrigin === "zcommerce") {
    code = "PHP";
  }

  Currencies.findOne({
    code : code
  }).exec(function (err, foundCurrency) {
    if (err) {
      return callback(err);
    } else if (!foundCurrency) {
      sails.log.error("Validation :: Global :: Users :: " + code + " currency not found.");
      return callback({
        type : "serverError"
      });
    }

    values.currencyId = foundCurrency.id;
    callback();
  });
}

