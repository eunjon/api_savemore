var base36 = require("monotonic-timestamp-base36");

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Global :: Orders :: " + (isUpdate ? "onUpdate" : "onCreate"));
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Validations :: Global :: Orders :: App type is undefined.");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        process.nextTick(seriesCb);
      },
    ], function (err) {
      if (err) {
        return callback(err);
      }

      delete values.__old;
      callback();
    });
  }

  async.series([
    function (seriesCb) {
      process.nextTick(seriesCb);
    },
  ], callback);
};
