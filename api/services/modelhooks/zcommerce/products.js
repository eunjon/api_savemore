/**
 * This hook assumes the values parameter has been validated.
 *
 * @param {object} values
 * @param {function} nodejs callback
 *
 */

var ObjectID = require("objectid");

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Hook :: Z-Commerce :: Products :: " + (isUpdate ? "onUpdate" : "onCreate"));
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Hooks :: Z-Commerce :: Products :: App type is undefined");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        removeAllResold(values, seriesCb);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      delete values.__old;
      callback();
    });
  }

  async.series([
    function (seriesCb) {
    }
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

function removeAllResold(values, callback) {
  //@TODOS add notification hooks to reselling shops.
  _.ensure(values, "__old.metadata.isResold", "");

  // skip if isResold flag is not from true to false;
  if (!(!values.metadata.isResold && values.__old.metadata.isResold)) {
    return process.nextTick(callback);
  }

  // skip if hasClones flag is false;
  if (!_.safe(values, "metadata.hasClones")) {
    return process.nextTick(callback);
  }

  Products.native(function (err, collection) {
    if (err) {
      return callback(err);
    }

    collection.updateMany({
      "metadata.sourceProductId" : values.id,
      appType : values.appType,
      isDeleted : false
    }, {
      isDeleted : true
    }, {
      multi : true
    }, function (err, result) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  });
}
