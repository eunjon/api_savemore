module.exports = function (values) {
  if (!values.appType) {
    sails.log.warn("No App type.", values);
  }

  _.each(values, function (value, key) {
    if (_.hasIn(value, "sanitize") && typeof value.sanitize === "function") {
      values[key] = value.sanitize(values.appType);
    }
  });
}
