var WLValidationError = require('waterline/lib/waterline/error/WLValidationError.js');

module.exports = {
  badRequest : function(message) {
    return {
      error : "E_BADREQUEST",
      status : 400,
      summary : message
    };
  },

  forbidden : function(message) {
    return {
      error : "E_FORBIDDEN",
      status : 403,
      summary : message
    };
  },

  serverError : function(message) {
    return {
      error : "E_INTERNAL",
      status : 500,
      summary : message
    };
  },

  notFound : function(message) {
    return {
      error : "E_NOTFOUND",
      status : 404,
      summary : message
    };
  },

  validation : function(invalidAttributes, status) {
    return new WLValidationError({
      invalidAttributes : invalidAttributes,
      status : status
    });
  }
};
