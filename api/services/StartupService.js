var fs = require("fs");
var underscore_str = require("underscore.string");
var underscore = require("underscore");

module.exports = function (cb) {
  async.series([
    //
    // Ensure indexes
    //
    function (callback) {
      Products.native(function (err, products) {
        if (err) {
          return callback(err);
        }
        products.ensureIndex({"randomPoint" : "2d"}, function (err, index) {
          if (err) {
            return callback(err);
          }
          callback();
        });
      });
    },
    function (callback) {
      Shops.native(function (err, shops) {
        if (err) {
          return callback(err);
        }
        shops.ensureIndex({"randomPoint" : "2d"}, function (err, index) {
          if (err) {
            return callback(err);
          }
          callback();
        });
      });
    },
    function (callback) {
      sails.log.info("Adding countries..")
      fs.readFile(
        sails.config.files.countries,
        "utf8",
        function (err, data) {
          if (err) {
            return callback(err);
          }
          try {
            data = JSON.parse(data);
          } catch (e) {
            return callback(e);
          }

          async.each(data, function (datum, eachCb) {
            Countries.findOrCreate({
              code : datum.code
            }, datum).exec(function (err, result) {
              if (err) {
                return eachCb(err);
              }
              eachCb();
            });
          }, function (err) {
            if (err) {
              return callback(err);
            }
            callback();
          })
        });
    },
    function (callback) {
      sails.log.info("Adding product categories for zcommerce.");
      async.waterfall([
        function (waterfallCb) {
          fs.readFile(sails.config.files["zcommerce"].productCategories,
          "utf8",
          function (err, data) {
            if (err) {
              return waterfallCb(err);
            }

            try {
              data = JSON.parse(data);
              data = _.map(data, function (elem) {
                elem.slug = underscore_str.slugify(elem.name.replace("'", ""));
                elem.subcategories = _.map(elem.subcategories, function (elem2) {
                  elem2.groupheader.slug = underscore_str.slugify(elem2.groupheader.name.replace("'", ""));
                  elem2.groupling = elem2.groupling.map(function (e) {
                    e.slug = underscore_str.slugify(e.name.replace("'", ""));
                    return e;
                  });

                  return elem2;
                });
                return elem;
              });
            } catch (e) {
              return callback(e);
            }

            waterfallCb(null, data);
          });
        },
        function (categories, waterfallCb) {
          Misc.findOrCreate({
            name : "Product Categories",
            appType : "zcommerce"
          }, {
            name : "Product Categories",
            metadata : categories,
            appType : "zcommerce"
          }).exec(function (err, result) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb()
          });
        }
      ], function (err) {
        if (err) {
          return callback(err);
        }

        callback();
      });
    },
    function (callback) {
      PatchService(callback);
    },
    function (callback) {
      sails.log.info("Creating admin account for zcommerce.");

      Users.findOne({
        username : sails.config.zcommerce.admin.username
      }).exec(function (err, foundUser) {
        if (err) {
          return callback(err);
        } else if (foundUser) {
          return callback();
        }

        Users.create(sails.config.zcommerce.admin)
          .exec(function (err, createdUser) {
            if (err) {
              return callback(err);
            }

            Shops.create({
              userId : createdUser.id
            }).exec(function (err, createdShop) {
              if (err) {
                return callback(err);
              }

              callback();
            });
          });
      });
    }
  ], function (err, result) {
    if (err) {
      return cb(err);
    }
    cb();
  })
}
