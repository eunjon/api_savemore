module.exports = function (cb) {
  sails.log.info("Applying patch..");
  async.series([
    function (callback) {
      callback();
    }
  ], function (err, result) {
    if (err) {
      return cb(err);
    }
    sails.log.info("Finished applying patch..");
    cb();
  })
}
