module.exports = require('include-all')({
  dirname     :  __dirname + '/validation',
  filter      :  /(.+)\.js$/,
  excludeDirs :  /^\.(git|svn)$/,
  optional    :  true
});
