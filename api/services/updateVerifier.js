module.exports = function (model, values) {
  var attr = _.mapValues(model._attributes, function (obj) {
    return obj.updateReadOnly;
  });

  _.each(values, function (val, key) {
    if (attr[key] || !attr.hasOwnProperty(key)) {
      throw new Error("Invalid attribute: " + key);
    }
  });

  return values;
}

