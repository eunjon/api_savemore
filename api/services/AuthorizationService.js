module.exports = {
  isAuthorized : function(options, callback) {
    var accessToken = options.token;
    if (accessToken && accessToken.indexOf("Bearer ") === 0) {
      accessToken = accessToken.replace("Bearer ", "");
    } else {
      return callback("You are not permitted to perform this action.");
    }

    Sessions.findOne({
      appType : options.appType,
      accessToken : accessToken
    }).exec(function(err, session) {
      if (err) {
        sails.log.error(err);
        return callback("Session token not found.");
      } else if (!session || (session && !session.userId)) {
        sails.log.verbose("Access Token : " + accessToken);
        sails.log.verbose("Session not found!");
        return callback("You are not permitted to perform this action.");
      }

      callback(null, session);
    });
  }
};
