var underscore = require("underscore");
var requestHttp = require("request");

module.exports = {
  pay : function (options, callback) {
    var payment = options.payment;
    var receivers = options.receivers;
    var currencyCode = options.currencyCode;
    var appType = options.appType;
    var seller = underscore.findWhere(receivers, {
      type : "sale"
    });
    var commissionRx = underscore.where(receivers, {
      type : "commission"
    });
    var sellerMerchantId = null;
    var response = null;

    if (!seller) {
      return callback("eCashService :: pay with ecash :: Seller info not found.");
    }

    if (!payment.nonDecimal) {
      payment.totalAmount = Math.ceil(payment.totalAmount * 100) / 100;
      payment.totalItemsAmount = Math.ceil(payment.totalItemsAmount * 100) / 100;
      payment.totalShipping = Math.ceil(payment.totalShipping * 100) / 100;
      payment.handlingFee = Math.ceil(payment.handlingFee * 100) / 100;
      payment.platformFee = Math.ceil(payment.platformFee * 100) / 100;
      payment.tax = Math.ceil(payment.tax * 100) / 100;
    }

    if (commissionRx.length) {
      commissionRx = commissionRx.map(function (com) {
        com.amount = Math.floor(com.amount * 100 ) / 100;
        return com;
      });
    }

    sails.log.verbose("Payment computation.");
    sails.log.verbose(payment);
    sails.log.verbose(commissionRx);

    async.series([
      function (cb) {
        Shops.findOneById(seller.shopId)
          .exec(function (err, shop) {
            if (err) {
              return cb(err);
            } else if (!shop) {
              return cb({
                type : "notFound",
                msg : "eCashService :: pay with ecash :: Seller shop not found."
              });
            }

            try {
              sellerMerchantId = shop.metadata[appType].eCashInfo.merchantId;
            } catch (e) {
              return cb(e);
            }

            cb();
          });
      },
      function (cb) {
        if (!commissionRx.length) {
          return process.nextTick(cb);
        }

        async.map(commissionRx, function (com, mapCb) {
          Shops.findOneById(com.shopId)
            .exec(function (err, foundShop) {
              if (err) {
                return mapCb(err);
              } else if (!foundShop) {
                return mapCb({
                  type : "notFound",
                  msg : "PaypalService :: PayWithPaypal :: Reseller shop not found."
                });
              }

              try {
                com.merchantId = shop.metadata[appType].eCashInfo.merchantId;
              } catch (e) {
                return mapCb("Reseller has no email address.");
              }

              mapCb(null, com);
            });
        }, function (err, mapResult) {
          if (err) {
            return cb(err);
          }

          commissionRx = mapResult;
          cb();
        });
      },
      function (cb) {
        requestHttp.post({
          url : sails.config.ecash.paymentUrl,
          formData : {
            api_token : sails.config.ecash.apiToken,
            api_key : sails.config.ecash.apiKey,
            merchant_id : sellerMerchantId,
            client_id : options.credentials.merchantId,
            client_pin : options.credentials.password,
            amount : payment.totalAmount
          }
        }, function (err, result, body) {
          if (err) {
            return cb(err);
          }

          try {
            body = JSON.parse(body);
          } catch (e) {
            sails.log.error("SessionsController :: _upsSession ::", e.message);
            return cb("Error.");
          }

          if (!body.S) {
            return cb({
              type : "insufficientFunds",
              msg : body.M
            });
          }

          sails.log.debug(body);

          response = {
            payKey : body.TN,
            message : body.M
          }

          cb();
        });
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback(null, response);
    });
  }
}
