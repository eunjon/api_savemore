var __ = require("lodash");

module.exports = {
  //
  // [0] argument is the object to be checked
  // [1..n-2] arguments are the properties
  //
  checkProps : function (obj, props) {
    var retVal = true;

    if (typeof obj !== "object" || !Array.isArray(props)) {
      sails.log.verbose("Utils::checkProps::Invalid parameters.");
      return false;
    }

    _.each(props, function (prop) {
      var hasProp = obj.hasOwnProperty(prop);

      if (!hasProp) {
        sails.log.error("Missing property", prop);
      }

      retVal = retVal && obj.hasOwnProperty(prop);
    });

    return retVal;
  },
  queryStringToJSON : function (str) {
    var pairs = null;
    var result = {};

    if (!str) {
      sails.log.error("queryStringToJSON::empty parameter");
      return "";
    }

    pairs = (str || location.search).slice(0).split('&');
    for (var idx in pairs) {
      var pair = pairs[idx].split('=');
      if (!!pair[0]) {
        result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
      }
    }

    return result;
  }
}
