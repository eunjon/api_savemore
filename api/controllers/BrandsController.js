/**
 * BrandsController
 *
 * @description :: Server-side logic for managing Brands
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create : function (req, res) {
    res.notFound();
  },

  find : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    try {
      if (req.query.where) {
        req.query.where = JSON.parse(req.query.where);
      } else {
        req.query.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return res.badRequest("Malformed query.");
    }

    Brands.count()
      .where(req.query.where)
      .exec(function (err, count) {
        if (err) {
          return res.serverError(err);
        } else if (!count) {
          return res.ok({
            count : 0,
            brands : []
          });
        }

        Brands.find(req.query)
          .exec(function (err, foundBrands) {
            if (err) {
              return res.serverError(err);
            }

            foundBrands = _.map(foundBrands, function (brand) {
              return brand.sanitize();
            });

            res.ok({
              count : count,
              brands : foundBrands
            });
          });
      });
  },

  findOne : function (req, res) {
    res.notFound();
  },

  update : function (req, res) {
    res.notFound();
  },

  destroy : function (req, res) {
    res.notFound();
  }
};

