/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {
  AuthorizationService.isAuthorized({
    token : req.headers.authorization,
    appType : req.appType
  }, function(error, session) {
    if (error) {
      return res.badRequest("You are not permitted to perform this action.");
    }

    // req.session.session = session;
    req.userId = session.userId;
    req.appType = session.appType;
    req.token = session.accessToken;
    next();
  });
};
