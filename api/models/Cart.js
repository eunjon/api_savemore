/**
* CartItems.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var ObjectID = require("objectid");
var underscore = require("underscore");

module.exports = {
  schema : true,

  types : {
    updateReadOnly : function () {
      return true;
    }
  },

  attributes: {
    userId : {
      model : "users",
      required : true,
      updateReadOnly : true
    },

    shopId : {
      model : "shops",
      required : true,
      updateReadOnly : true
    },

    productId : {
      model : "products",
      required : true
    },

    quantity : {
      type : "integer",
      required : true
    },

    status : {
      type : "string",
      enum : ["added", "prepurchased"],
      defaultsTo : "added",
      updateReadOnly : true
    },

    metadata : {
      type : "json"
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true,
      updateReadOnly : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false,
      updateReadOnly : true
    },

    sanitize : function () {
      var cartItem = this.toObject();

      delete cartItem.status;
      delete cartItem.appType;
      delete cartItem.isDeleted;

      return cartItem;
    },

    /**
     * Fetches the seller info of this product.
     *
     * @method     fetchSeller
     * @param      {object}    options   { description }
     * @param      {Function}  callback  { description }
     * @return     {<type>}    { description_of_the_return_value }
     */
    fetchSeller : function (options, callback) {
      var item = this;

      if (typeof callback !== "function") {
        throw new Error("Callback is not a function.");
      } else if (!Utils.checkProps(options, [])) {
        sails.log.error("Cart :: fetchSeller :: Missing parameters.");
        return callback({
          type : "serverError",
          msg : ""
        });
      }

      Users.findOne({
        id : item.productId.userId,
        isDeleted : false
      }).exec(function (err, foundUser) {
        if (err) {
          return callback(err);
        } else if (!foundUser) {
          sails.log.error("Cart :: fetchSeller :: Seller not found.");
          return callback({
            type : "serverError",
            msg : ""
          });
        }

        item.productId.userId = foundUser.profileSanitize();
        callback();
      });
    },

    /**
     * Marks the cart items as prepurchased and subtract product quantity.
     *
     * @method     processInventory
     * @param      {object}    options   { description }
     * @param      {Function}  callback  { description }
     */
    processInventory : function (options, callback) {
      var item = this;

      if (typeof callback !== "function") {
        throw new Error("Callback is not a function.");
      } else if (!Utils.checkProps(options, [])) {
        sails.log.error("Cart :: processInventory :: Missing parameters.");
        return callback({
          type : "serverError",
          msg : ""
        });
      }

      Products.findOne({
        id : item.productId.id,
        isDeleted : false
      }).exec(function (err, product) {
        if (err) {
          return callback(err);
        } else if (!product) {
          return callback("Product not found.");
        } else if (product.quantity < item.quantity) {
          return callback("Not enough quantity.");
        }

        product.quantity -= item.quantity;
        product.save(function (err) {
          if (err) {
            return callback(err);
          }

          item.status = "prepurchased";
          item.save(function (err) {
            if (err) {
              return callback(err);
            }

            callback();
          });
        });
      });
    },

    /**
     * Computes the tag commission for this item, if applicable.
     *
     * @method     computeTagCommission
     * @param      {<type>}    options   { description }
     */
    computeTagCommission : function (options) {
      var item = this;
      var product = item.productId;
      var currency = product.currency;
      var computation = {
        grossAmount : item.quantity * product.price,
        netAmount : item.quantity * product.price,
        netReceive : item.quantity * product.price,
        tagCommission : 0,
        resellCommission : 0
      };

      if (!Utils.checkProps(options, [])) {
        sails.log.error("Cart :: computeResellCommission :: Missing parameters.");
        throw new Error("Missing parameters.");
      } else if (!item.tagger || item.resoldBy) {
        item.computation = computation;
        return;
      }

      if (product.tagCommission.type === "percentage") {
        if (currency.nonDecimal) {
          computation.tagCommission = Math.floor(computation.grossAmount * (product.tagCommission.value / 100));
        } else {
          computation.tagCommission = Math.floor(computation.grossAmount * (product.tagCommission.value)) / 100;
        }
      } else if (product.tagCommission.type === "fixed") {
         if (currency.nonDecimal) {
          computation.tagCommission = Math.floor(product.tagCommission.value);
         } else {
          computation.tagCommission = product.tagCommission.value;
         }
      } else {
        sails.log.error("Cart :: computeTagCommission :: Invalid tag commission type.");
        throw new Error("Error.");
      }

      computation.netReceive -= computation.tagCommission;
      item.computation = computation;
    },

    /**
     * Computes the resell commission for this item, if applicable.
     *
     * @method     computeResellCommission
     * @param      {<type>}    options   { description }
     */
    computeResellCommission : function (options) {
      var item = this;
      var product = item.productId;
      var currency = product.currency;
      var computation = {
        grossAmount : item.quantity * product.price,
        netAmount : item.quantity * product.price,
        netReceive : item.quantity * product.price,
        tagCommission : 0,
        resellCommission : 0
      };

      if (!Utils.checkProps(options, [])) {
        sails.log.error("Cart :: computeResellCommission :: Missing parameters.");
        throw new Error("Missing parameters.");
      } else if (item.tagger || !item.resoldBy) {
        item.computation = computation;
        return;
      }

      if (product.resellCommission.type === "percentage") {
        if (currency.nonDecimal) {
          computation.resellCommission = Math.floor(computation.grossAmount * (product.resellCommission.value / 100));
        } else {
          computation.resellCommission = Math.floor(computation.grossAmount * (product.resellCommission.value)) / 100;
        }
      } else if (product.resellCommission.type === "fixed") {
         if (currency.nonDecimal) {
          computation.resellCommission = Math.floor(product.resellCommission.value);
         } else {
          computation.resellCommission = product.resellCommission.value;
         }
      } else {
        sails.log.error("Cart :: computeResellCommission :: Invalid tag commission type.");
        throw new Error("Error.");
      }

      computation.netReceive -= computation.resellCommission;
      item.computation = computation;
    },

    /**
     * Compute the shipping cost for this item.
     *
     * @method     computeShippingCost
     * @param      {<type>}    options   { description }
     * @param      {Function}  callback  { description }
     */
    computeShippingCost : function (options, callback) {
      var item = this;
      var computation = item.computation;

      if (typeof callback !== "function") {
        throw new Error("Callback is not a function.");
      } else if (!Utils.checkProps(options, ["shippingId", "selectedShop", "hasOtherItems"])) {
        sails.log.error("Cart :: computeResellCommission :: Missing parameters.");
        return callback({
          type : "serverError",
          msg : ""
        });
      } else if (_.isEmpty(item.computation)) {
        sails.log.error("Cart :: computeShippingCost :: Item computation object is missing.");
        return callback({
          type : "serverError",
          msg : ""
        });
      }

      Shipping.findOne({
        id : options.shippingId,
        isDeleted : false
      }).exec(function (err, shipping) {
        if (err) {
          return callback({
            type : "serverError",
            msg : err
          });
        } else if (!shipping) {
          return callback({
            type : "notFound",
            msg : "Shipping address not found."
          });
        }

        try {
          var productShippingRates = _.safe(item, "productId.metadata.shippingData", []);
          var cost = null;

          if (!productShippingRates.length) {
            sails.log.error("Shipping data not found.");
            throw new Error("Error");
          }

          cost = underscore.findWhere(productShippingRates, {
            location : shipping.address.province
          });

          if (!cost) {
            cost = productShippingRates[0];
          }

          computation.netAmount += parseFloat(cost.singleCost);
          computation.netReceive += parseFloat(cost.singleCost);
          computation.shippingCost = parseFloat(cost.singleCost);
        } catch (e) {
          sails.log.error(e);
          return callback("Error.");
        }

        callback();
      });
    }
  }
};
