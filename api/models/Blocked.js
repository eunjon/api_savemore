/**
* Blocked.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    blockerId : {
      model : "users",
      required : true
    },

    blockedId : {
      model : "users",
      required : true
    },

    appType : {
      type : "string",
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    }
  }
};

