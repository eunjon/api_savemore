/**
 * Rooms.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema : true,

  attributes: {
    name : {
      type : "string",
      required : true
    },

    users : {
      type : "array",
      required : true
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    messageCount : {
      type : "integer",
      defaultsTo : 0
    },

    unreadCount : {
      type : "integer",
      defaultsTo : 0
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var room = this.toObject();

      delete room.appType;
      delete room.isDeleted;

      return room;
    }
  }
};

